#include <stdlib.h>
#include <GL/glut.h>
#include <GL/glu.h>
#include <iostream>

using namespace std;
GLUquadric *qobj;
GLUquadricObj *quadratic;

void drawGlasses(float x, float y);

void init(void) {
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);

    glShadeModel(GL_SMOOTH);
    qobj = gluNewQuadric();
    //quadric = gluNewQuadric();
    gluQuadricNormals(qobj, GLU_SMOOTH);
}

void display(void) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    gluLookAt(0.0, 1.75, 1.f, 0.0, 0.0, -1.0, 0.0, 1.0, 0.0);
    glTranslatef(0.f, 0.25f, 0.f);

    //draw a table
    glColor3ub(107, 73, 51);

    glPushMatrix();
    glTranslatef(-.9f, -0.8f, -.6f);
    glScalef(0.1f, 1.f, 0.1f);
    glutSolidCube(1.f);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(+.9f, -0.8f, -.6f);
    glScalef(0.1f, 1.f, 0.1f);
    glutSolidCube(1.f);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0.0f, -0.40f, -1.5f);
    glScalef(3.f, 0.05f, 2);
    glutSolidCube(1.f);
    glPopMatrix();

    // draw glasses
    float y = -0.2f;
    float x = -0.3f;
    drawGlasses(x, y);
    x = -x;
    drawGlasses(x, y);


    glColor3ub(71, 103, 40);


    y = -0.7;
    x = 0;
    glPushMatrix();
    glRotated(70, 1, 0, 0);
    glTranslatef(x, y, -.5f);
    glScaled(1.5f, 1, 1);
    gluCylinder(quadratic, .1, .1, .45f, 32, 32);
    glPopMatrix();


    x = 0;
    glPushMatrix();
    glRotated(70, 1, 0, 0);
    glTranslatef(x, y, -.7f);
    glScaled(1.5f, 1, 1);
    gluCylinder(quadratic, .01, .1, .20f, 32, 32);
    glPopMatrix();

    x = 0;
    glPushMatrix();
    glRotated(70, 1, 0, 0);
    glTranslatef(x, y, -.8f);
    glScaled(0.5f, 1, 1);
    gluCylinder(quadratic, .1, .1, .15f, 32, 32);
    glPopMatrix();

    ////draw a floor
    glColor3ub(126, 126, 125);
    glPushMatrix();
    glTranslatef(0.0f, -3.0f, 0.2f);
    glScalef(15.f, 0.1f, 25.f);
    glutSolidCube(1.f);
    glPopMatrix();
    glutSwapBuffers();
}

void drawGlasses(float x, float y) {
    glColor3ub(183, 183, 195);

    glPushMatrix();
    glRotated(70, 1, 0, 0);
    glTranslatef(x, y, -.5f);
    glScaled(0.5f, 1, 1);
    gluCylinder(quadratic, .1, .1, .15f, 32, 32);
    glPopMatrix();

    glPushMatrix();
    glRotated(70, 1, 0, 0);
    glTranslatef(x, y, -.4f);
    glScaled(0.2f, 0.5f, 1);
    gluCylinder(quadratic, .1, .1, .14f, 32, 32);
    glPopMatrix();

    y += 0.1f;
    glPushMatrix();
    glRotated(49, 1, 0, 0);
    glTranslatef(x, y, -.3f);
    glScaled(0.8f, 1, 1);
    gluCylinder(quadratic, .1, .1, .025f, 32, 32);
    glPopMatrix();
}

void reshape(int w, int h) {
    glViewport(0, 0, (GLsizei) w, (GLsizei) h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (GLfloat) w / (GLfloat) h, 1.0, 20.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

}

void keyboard(unsigned char key, int x, int y) {
    switch (key) {
        //turn on/off lights
        case 'q':
        case 'Q':
            gluDeleteQuadric(qobj);
            exit(0);
            break;
    }
    glutPostRedisplay();
}

int main(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800, 800);
    glutInitWindowPosition(0, 0);
    glutCreateWindow(argv[0]);
    init();
    glutDisplayFunc(display);
    glutIdleFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    quadratic = gluNewQuadric();
    glutMainLoop();
    return 0;
}
